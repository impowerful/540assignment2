import paho.mqtt.client as mqtt
import time
import json
import datetime

broker_hostname = "localhost"
port = 1883

motion_var = False

def on_connect(client, userdata, flags, return_code):
    print("CONNACK received with code %s." % return_code)
    if return_code == 0:
        print("connected")
    else:
        print("could not connect, return code:", return_code)

client = mqtt.Client(client_id="Publisher", userdata=None)
client.on_connect = on_connect

client.username_pw_set(username="username1", password="password1")

client.connect(broker_hostname, port, 60)
client.loop_start()

try:
    while True:
        motion_var = True
        
        if motion_var:  # Simulated motion detected
            timestamp = datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
            message = {
                "motion_detected": True,
                "timestamp": timestamp,
                "picture": "url_new_picture"
            }
            topic = "motion_detection"
            payload = json.dumps(message)
            result = client.publish(topic=topic, payload=payload)
            status = result.rc
            if status == mqtt.MQTT_ERR_SUCCESS:
                print("Message is published to topic " + topic)
            else:
                print("Failed to send message to topic " + topic)

        time.sleep(2)  # Simulate motion detection every 2 second
        
        motion_var = False
        
        time.sleep(2)
        
except KeyboardInterrupt:
    pass
finally:
    client.loop_stop()
    client.disconnect()