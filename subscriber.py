import paho.mqtt.client as mqtt
import time
import json
import datetime

def on_connect(client, userdata, flags, return_code):
    if return_code == 0:
        print("Connected to MQTT broker")
        client.subscribe("motion_detection")
    else:
        print("Could not connect, return code:", return_code)

def on_message(client, userdata, message):
    payload = json.loads(message.payload)
    timestamp = payload["timestamp"]
    picture_url = payload["picture"]
    topic = message.topic
    event_message = f"{timestamp} new event detected in {topic}: movement in room: {picture_url}"

    with open("motion_events.log", "a") as log_file:
        log_file.write(event_message + "\n")
    print(event_message)

broker_hostname = "localhost"
port = 1883

client = mqtt.Client("Subscriber")
client.username_pw_set(username="username1", password="password1")
client.on_connect = on_connect
client.on_message = on_message

client.connect(broker_hostname, port)
client.loop_start()

try:
    while True:
        time.sleep(10)
except KeyboardInterrupt:
    pass
finally:
    client.loop_stop()
    client.disconnect()